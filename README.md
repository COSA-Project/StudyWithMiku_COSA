# StudyWithMiku

STUDY WITH MIKU Web Version Cover

本分支由冷星维护[版本代号(COSA Ver.)]

本分支含有附加内容，并与WenqiOfficial主分支保持同步更新，<del>虽然经常忘记</del>

突发奇想，将其变为现实

[WEB](https://studymiku.wenqi.icu)

[WEB(Cloudflare)](https://studymiku.cloudflare.wenqi.icu)

[WEB(COSA Ver.)(CloudFlare)](https://miku.hanze.eu.org)

## 目前实现的功能

- [x] STUDY WITH MIKU 音乐

- [x] 背景视频

- [x] 全屏显示

- [x] 一言Tips

- [x] 丝滑的动画

- [x] 本地学习时长记录

- [x] 本地学习总时长记录

- [x] 单次学习共休息时长记录

- [x] 摸鱼自动停止计时(窗口失焦)

- [x] Tips显示设置

- [x] 自定义歌单

- [x] 当前在线人数

## 附加内容

- [x] 添加MikuTap
 
## 部署TIPS

### Umami

自行部署的话建议将 `<head>` 中的 Umami 统计代码更改为自行部署的统计器

```
<!-- Umami START -->
  <!-- 更改为你自己部署的Umami统计器 -->
<!-- Umami END -->
```

也别忘了去修改`mikuplayer`中的**Token**、**URL**值

---

[![Cover](https://raw.githubusercontent.com/WenqiOfficial/wenqicdn/master/img/banner/studymiku.jpg)](https://www.bilibili.com/video/BV1rV41157DR)
